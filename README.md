# README #

Ongoing implementation of the Matasano crypto challenges as described at http://cryptopals.com .

All problems will be solved in Python3. I will try and avoid using common libraries for all crypto-related utilities, to have more fun :-)

**Note**: this has been moved to my account on GitHub.